---
layout: page
title: Projects
comments: false
permalink: /projects/
---

* content
{:toc}

## Work projects
---

### Perun
* Started: 2010-present
* Website: [https://perun.cesnet.cz/](https://perun.cesnet.cz/)
* Sources: [GitHub](https://github.com/CESNET/perun.git)

Perun is an identity and access management system primary targeting academia environment. Perun is well suited for managing users within organizations and projects and managing access rights to the services.

### Perun WUI
* Started: 2014-present
* Website: [https://perun.cesnet.cz/](https://perun.cesnet.cz/)
* Sources: [GitHub](https://github.com/CESNET/perun-wui.git)

This project aims to deliver rich user graphical interface for Perun components. Project uses enterprise frameworks to make it work.

## Personal projects
---

### Arc like theme for Intellij IDEA
* Started: 2019-present
* Source: [GitLab](https://gitlab.com/zlamalp/arc-theme-idea.git)
* Download: [IntelliJ plugins website](https://plugins.jetbrains.com/plugin/12451-arc-theme)

This plugin bring Arc like theme for Intellij IDEA IDEs. Contains only light version!


### LDAP & LDIF plugin for Intellij IDEA
* Started: 2017-present
* Sources: [GitLab](https://gitlab.com/zlamalp/intellij-plugin-ldif.git)

This plugin for Intellij IDEA 2019.1+ brings support for LDIF file format and LDAP directory browsing and modification using same way as DB plugins do.

### Perun 4
* Started: 2016-present

Private project for testing different technologies and solutions, mainly later used in Perun 3.
