---
layout: page
title: About
comments: false
permalink: /about/
---

* content
{:toc}

## About me
---

### Work

<br />

#### 2012 → now

<br />

| Occupation or position held | Researcher, Network Identity Divison |
| Main activities and responsibilities | Research and development mainly in identity management field |
| Name and address of employer | CESNET z. s. p. o., Zikova 4, 160 00 Prague (Czech Republic) |
| Type of business or sector | Research and Development in IT |

<br />

### Skils

Experiences with developing Identity management and access management related systems. Knowledge of programming languages Java and Perl, collaboration tools like GIT and Maven, tools for continous integration and deployment like Jenkins. Knowledge of Spring and GWT frameworks for developing enterprise applications. Knowledge of user interface design principles. Knowledge of SQL database systems.

<br />

### School

<br />

#### 2012 - 2014 

| Title of qualification awarded | Master's degree |
| Principal subjects / occupational skills covered | Object-oriented methods for design of information systems. Software engeneering, computer architecture, communication and parallelism, user interface design principles and usability evaluation |
| Name and type of organisation providing education and training | Faculty of Informatics, Masaryk University, Žerotínovo nám. 9, 60177 Brno (Czech Republic) |

<br />

#### 2008 - 2012

| Title of qualification awarded | Bachelor's degree |
| Principal subjects / occupational skills covered | Analysis and design of the systems, authentication and authorization, principles of programming languages, security in information technologies, architecture of database systems, computer networks, operation systems, czech law in informatics |
| Name and type of organisation providing education and training | Faculty of Informatics, Masaryk University, Žerotínovo nám. 9, 60177 Brno (Czech Republic) |


