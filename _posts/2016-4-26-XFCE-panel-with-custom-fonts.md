---
layout: post
comments: true
categories: 
- xfce
- xubuntu
---

Some panel applets allows you to change font (weather), but some doesn't (clock, whiskermenu). As result panel is displayed inconsistently. But you can simply use HTML syntax to force panel display texts with custom fonts, colors etc.

Simply put any text inside &lt;span&gt; tag with appropriate parameters like font and color.

<div style="clear: both; text-align: center;">
    <img src="/downloads/whiskermenu.png">
</div>