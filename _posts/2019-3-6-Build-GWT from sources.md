---
layout: post
comments: true
categories: GWT
---

Since there is no next -SNAPSHOT version of GWT, you can build yourself a version from master. Just do following:


```
mkdir gwtproject
cd gwtproject
git clone https://gwt.googlesource.com/gwt gwt
git clone https://github.com/gwtproject/tools.git tools
cd gwt
export GWT_VERSION=2.9.0-SNAPSHOT && ant clean elemental dist-dev
```

Then you can use maven to install GWT into your local repository

```
maven/push-gwt.sh
```

