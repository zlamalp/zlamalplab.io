---
layout: post
comments: true
categories: ubuntu-touch ubports
---

In order to develop apps for ubuntu touch you will need a lxd container and android tools. Using *Ubuntu-SDK* is now deprecated, since ubports is not able to support it and it also doesn't worked for me on 16.04.

On Ubutnu 16.04 install required sw and init lxd like this:

<pre class="codebox nohighlight">
sudo apt-get install lxd android-tools-adb
sudo lxd init
</pre>

I used defaults for lxd init, and set only IPV4 network (let it do automatically).

### Install Clickable

You will need *Clickable* from Git and init lxd with it. It creates armhf image of ubuntu 15.04. It will download aprox 1GB of files and prepare build environment.
It's also later used to build and start the app on your phone (swotched to developer mode and connected by USB).

<pre class="codebox nohighlight">
cd ~
git clone https://github.com/bhdouglass/clickable.git
echo "export PATH=\$PATH:~/clickable" >> ~/.bashrc
. ~/.bashrc
clickable setup-lxd
</pre>

### Install and run template app

There is an empty app template provided by ubports, which you can clone and run on your ubuntu phone device.

<pre class="codebox nohighlight">
git clone https://github.com/bhdouglass/ut-app-template
cd ut-app-template/
</pre>

Make sure your phone is switched to *Developer mode* in *System settings*. Then you can build and start the app just by *clickable* command, you will see output like this and app is started on you phone.

<pre class="codebox nohighlight">
user@host:~$ clickable
* daemon not running. starting it now on port 5037 *
* daemon started successfully *
pkill: killing pid 27315 failed: Operation not permitted
-- The C compiler identification is GNU 4.9.2
-- The CXX compiler identification is GNU 4.9.2
-- Check for working C compiler: /usr/bin/arm-linux-gnueabihf-gcc
-- Check for working C compiler: /usr/bin/arm-linux-gnueabihf-gcc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/arm-linux-gnueabihf-g++
-- Check for working CXX compiler: /usr/bin/arm-linux-gnueabihf-g++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Found Gettext: /usr/bin/msgmerge (found version "0.19.2") 
-- Configuring done
-- Generating done
-- Build files have been written to: /home/zlamalp/Code/ut-app-template/build
Scanning dependencies of target Template_automoc
Scanning dependencies of target app.desktop
Scanning dependencies of target app.pot
[ 16%] [ 50%] [ 50%] Automatic moc for target Template
Generating translation template
Merging translations into app.desktop...
Generating moc_plugin.cpp
Wrote app.desktop.in.h
Merging translations into app.desktop.
[ 50%] [ 50%] Built target app.desktop
Built target app.pot
Generating moc_template.cpp
[ 50%] Built target Template_automoc
Scanning dependencies of target Template
[100%] [100%] [100%] Building CXX object plugins/Template/CMakeFiles/Template.dir/Template_automoc.cpp.o
Building CXX object plugins/Template/CMakeFiles/Template.dir/plugin.cpp.o
Building CXX object plugins/Template/CMakeFiles/Template.dir/template.cpp.o
Linking CXX shared module Template/libTemplate.so
[100%] Built target Template
[ 16%] Merging translations into app.desktop...
Merging translations into app.desktop.
[ 16%] Built target app.desktop
[ 33%] Generating translation template
Wrote app.desktop.in.h
[ 33%] Built target app.pot
[ 50%] Automatic moc for target Template
[ 50%] Built target Template_automoc
[100%] Built target Template
Install the project...
-- Install configuration: ""
-- Installing: /home/zlamalp/Code/ut-app-template/build/tmp//manifest.json
-- Installing: /home/zlamalp/Code/ut-app-template/build/tmp//app.apparmor
-- Installing: /home/zlamalp/Code/ut-app-template/build/tmp//qml
-- Installing: /home/zlamalp/Code/ut-app-template/build/tmp//qml/Main.qml
-- Installing: /home/zlamalp/Code/ut-app-template/build/tmp//assets
-- Installing: /home/zlamalp/Code/ut-app-template/build/tmp//assets/logo.png
-- Installing: /home/zlamalp/Code/ut-app-template/build/tmp//app.desktop
-- Installing: /home/zlamalp/Code/ut-app-template/build/tmp/lib/arm-linux-gnueabihf/Template/libTemplate.so
-- Installing: /home/zlamalp/Code/ut-app-template/build/tmp/lib/arm-linux-gnueabihf/Template/qmldir
Successfully built package in './app.bhdouglass_1.0_armhf.click'.
977 KB/s (40164 bytes in 0.040s)
Installing files              [=========================]         
Finished                      [=========================]         
Installing files              [=========================]         
Starting                      [=========================]         
Finished                      [=========================]         
Installed   	app.bhdouglass-1.0.armhf (installed:click,removable=1,app_name=app)	summary goes here
</pre>

### Existing core apps

If you wish to hack the core apps of ubuntu touch, you can find source at Github under ubports account: https://github.com/ubports

They are mostly combination of C++/QML, so in order to build and run the apps you will need to install many packages like make, cmake, intltools etc. and of course QT packages.

Also not all of them support *.click* packaging, so *clickable* doesn't seem to work with them.