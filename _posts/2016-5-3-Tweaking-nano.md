---
layout: post
comments: false
categories: tweak
---

So, you can finally make my favorite text editor nano a useful editor with content wrapping and syntax highlighting. Simply create .nanorc file located in you home folder and put in following content:

    include /usr/share/nano/sh.nanorc
    include /usr/share/nano/css.nanorc
    include /usr/share/nano/html.nanorc
    include /usr/share/nano/c.nanorc
    include /usr/share/nano/nanorc.nanorc
    include /usr/share/nano/java.nanorc
    include /usr/share/nano/debian.nanorc
    include /usr/share/nano/perl.nanorc
    include /usr/share/nano/xml.nanorc
    include /usr/share/nano/tex.nanorc
    include /usr/share/nano/patch.nanorc
    include /usr/share/nano/man.nanorc
    include /usr/share/nano/groff.nanorc
    include /usr/share/nano/python.nanorc
    include /usr/share/nano/ruby.nanorc
    include /usr/share/nano/asm.nanorc

    set softwrap
    set undo
    set autoindent
    set tabsize 4

For more syntax highlighting definitions you can search the internet: http://code.google.com/p/nanosyntax/source/browse/trunk/syntax-nanorc/.