---
layout: post
comments: false
categories:
- xfce
- xubuntu
- tweak
---

You can install custom console themes into xfce4-terminal from https://terminal.sexy/ It has nice export function, but it doesn't provide correct config file content for the app on Ubuntu 16.04.

Original exported version:
<pre class="codebox nohighlight">
[Configuration]
ColorCursor=#c5c5c8c8c6c6
ColorForeground=#c5c5c8c8c6c6
ColorBackground=#1d1d1f1f2121
ColorPalette=#1d1d1f1f2121;#cccc34342b2b;#191988884444;#fbfba9a92222;#39397171eded;#a3a36a6ac7c7;#39397171eded;#c5c5c8c8c6c6;#969698989696;#cccc34342b2b;#191988884444;#fbfba9a92222;#39397171eded;#a3a36a6ac7c7;#39397171eded;#ffffffffffff
</pre>

You need to modify header

* Remove ``[Configuration]`` and replace it with ``[Scheme]``. 
* Then define themes name by ``Name=your_name``. 
* Theme file needs to have ``.theme`` suffix.
* For system wide installation put it in ``/usr/share/xfce4/terminal/colorschemes/``.

Fixed version of Google dark working in xfce4-terminal:
<pre class="codebox nohighlight">
[Scheme]
Name=Google dark
Name[cs]=Google dark
ColorCursor=#c5c5c8c8c6c6
ColorForeground=#c5c5c8c8c6c6
ColorBackground=#1d1d1f1f2121
ColorPalette=#1d1d1f1f2121;#cccc34342b2b;#191988884444;#fbfba9a92222;#39397171eded;#a3a36a6ac7c7;#39397171eded;#c5c5c8c8c6c6;#969698989696;#cccc34342b2b;#191988884444;#fbfba9a92222;#39397171eded;#a3a36a6ac7c7;#39397171eded;#ffffffffffff
</pre>

Then you no longer need to download and run base16 shell scripts to colorize your terminal, just use native scheme.