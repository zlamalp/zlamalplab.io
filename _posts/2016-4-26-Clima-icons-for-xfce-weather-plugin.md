---
layout: post
comments: false
categories: xfce
---

I found very nice icons in a flat design for XFCE panel weather plugin.

* Install

<pre class="codebox nohighlight">
wget https://zlamalp.gitlab.io/downloads/Clima_Weather_Icons.tar.gz
tar -x -f ./Clima_Weather_Icons.tar.gz
cp -r Clima/ ~/.config/xfce4/weather/icons/
rm -r Clima/
rm Clima_Weather_Icons.tar.gz
</pre>

* Change setting in xfce-weather-plugin

<div style="clear: both; text-align: center;">
    <img src="/downloads/clima.png" />
</div>
