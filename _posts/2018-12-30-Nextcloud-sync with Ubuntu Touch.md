---
layout: post
comments: false
categories: 
 - ubuntu-touch
 - ubports
---

Finally I got it working with my local Nextcloud instance. Problems were:

* Self-signed certificate on nextcloud server
* UTouch can't sync contacts from own/nextcloud

Since I have nextcloud installed on my Turris omnia router, first problem was, that autmatically generated certificate is missing "commonName" attribute. It resulted in error, that I couldn't add Nextcloud account on the phone at all.

So I generated new cetificate and add it to the lighttpd (http server) with my local IP address (192.168.1.1) as commonName attribute. Then I was able to add nextcloud account as usual. But it stil failed to sync my calendars, since the certificate was not trusted by the phone.

So I pushed the certificate to the phone using ``adb push cert.pem /home/phablet/``.

Then I entered remote shell on the phone using ``adb shell`` and switched to the root by ``sudo su -``.  You need to have developer mode enabled for all of this.

Then I copied cert to the system folder (you need to remount filesistem for read/write - eg. using UT tweak tool) - ``cp cert.pem /usr/share/ca-certificates/`` and then I reconfigured system certificates: ``dpkg-reconfigure ca-certificates``. When asked for what to do I selected "ask" option. Then I was presented with all certificates on the system and selected my new cert as trusted with the others. Then they are all compiled to /etc/ssl/certs/ca-certificates.crt, which is a system store. Then voila, and calendars were synced as usuall.

Last step was to sync contacts. For this I picked parts of the script presented on ubports wiki, which is for syncing both contacts and calendars, but it never really worked for me. I filled by credentials, system and display names of contact address book and URL for my contacts (carDAV server).

```sh
#!/bin/bash
# This script is a draft combination of the script found at https://gist.github.com/tcarrondo
# It is more or less to remember what I have done to make it work for my Fairphone 2 with UBports ubuntu touch
# Combined by me: Sebastian Gallehr <sebastian@gallehr.de>
# Thanks to: Tiago Carrondo <tcarrondo@ubuntu.com>
# Thanks to: Romain Fluttaz <romain@botux.fr>
# Thanks to: Wayne Ward <info@wayneward.co.uk>
# Thanks to: Mitchell Reese <mitchell@curiouslegends.com.au>
# --------------- [ Server ] ---------------- #
USERNAME=""               # you know this one
PASSWORD=""               # lots of ******

# ----------------- [ Phone ] ----------------- #

CONTACTS_CONFIG_NAME="nextcloudcontacts"            # I use "myCloud"
CONTACTS_NAME="nextcloudcontacts"          # I use "personalcontacts"
CONTACTS_VISUAL_NAME="Nextcloud"           # you can choose a nice name to show on the contacts app like "OwnContacts"
CONTACTS_URL="https://192.168.1.1/nextcloud/remote.php/dav/addressbooks/users/[username]/contacts/"    # CardDAV url to my contacts taken from OwnCloud web interface
CRON_FREQUENCY="hourly"        # I use "hourly"

export DBUS_SESSION_BUS_ADDRESS=$(ps -u phablet e | grep -Eo 'dbus-daemon.*address=unix:abstract=/tmp/dbus-[A-Za-z0-9]{10}' | tail -c35)

#Create contact list
syncevolution --create-database backend=evolution-contacts database=$CONTACTS_VISUAL_NAME
#Create Peer
syncevolution --configure --template webdav username=$USERNAME password=$PASSWORD syncURL=$CONTACTS_URL keyring=no target-config@$CONTACTS_CONFIG_NAME
#Create New Source
syncevolution --configure backend=evolution-contacts database=$CONTACTS_VISUAL_NAME @default $CONTACTS_NAME
#Add remote database
syncevolution --configure database=$CONTACTS_URL backend=carddav target-config@$CONTACTS_CONFIG_NAME $CONTACTS_NAME
#Connect remote contact list with local databases
syncevolution --configure --template SyncEvolution_Client Sync=None syncURL=local://@$CONTACTS_CONFIG_NAME $CONTACTS_CONFIG_NAME $CONTACTS_NAME
#Add local database to the source
syncevolution --configure sync=two-way backend=evolution-contacts database=$CONTACTS_VISUAL_NAME $CONTACTS_CONFIG_NAME $CONTACTS_NAME
#Start first sync
syncevolution --sync refresh-from-remote $CONTACTS_CONFIG_NAME $CONTACTS_NAME

#Add Sync Cronjob
sudo mount / -o remount,rw
COMMAND_LINE="export DISPLAY=:0.0 && export DBUS_SESSION_BUS_ADDRESS=$(ps -u phablet e | grep -Eo 'dbus-daemon.*address=unix:abstract=/tmp/dbus-[A-Za-z0-9]{10}' | tail -c35) && /usr/bin/syncevolution $CONTACTS_NAME"
sudo sh -c "echo '$COMMAND_LINE' > /sbin/sogosync"
sudo chmod +x /sbin/sogosync

CRON_LINE="@$CRON_FREQUENCY /sbin/sogosync"
(crontab -u phablet -r;) # only if no other cronjob already exist in the crontab
(crontab -u phablet -l; echo "$CRON_LINE" ) | crontab -u phablet -
sudo mount / -o remount,ro
sudo service cron restart
```

Then you need to push this file to the phone again: ``adb push dav.sh /home/phablet/`` and enter phone shell using ``adb shell``. Then you make file executable and execute it using ``chmod +x dav.sh`` and ``./dav.sh``. You will be asked for sudo password during the process. You contacts will be sinced every hour by the cron service, but you can manually start it with entering the phone shell and executing ``/sbin/sogosync``.

And its done, contacts are there :-).