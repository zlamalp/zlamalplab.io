---
layout: post
comments: true
categories:
- personal
- security
---

This is a simple guide how you can use your Yubikey for SSH authentication, so that your key is not stored on disc but rather on token (yubikey in this case). Expected environment is Ubuntu 18.04 LTS.

### Configure your system

First install necessary packages.
```
sudo apt install pcscd scdaemon pinentry-gnome3 yubikey-personalization yubikey-personalization-gui
```

Then copy udev rules for yubikey so it can work as smart card.
```
sudo cp /lib/udev/rules.d/69-yubikey.rules /etc/udev/rules.d/69-yubikey.rules
```

We will need to enable SSH support in GPG agent, hence we want to start it with custom params. So first we will deactivate default GPG agent service for you user.
```
systemctl --global --user mask --now gpg-agent.service gpg-agent.socket gpg-agent-ssh.socket gpg-agent-extra.socket gpg-agent-browser.socket
```

Then we will manually start GPG agent on user login (through X sever)
```
cat >>~/.xsessionrc <<"EOF"
gpg-agent --daemon --enable-ssh-support --default-cache-ttl-ssh 36000 --max-cache-ttl-ssh 72000 --pinentry-program /usr/bin/pinentry-gnome3
EOF
```

And we need to export SSH AUTH socket of GPG agent for your user console (replace 1000 with your UID). 
```
cat >>~/.bashrc <<"EOF"
SSH_AUTH_SOCK=/run/user/1000/gnupg/S.gpg-agent.ssh; export SSH_AUTH_SOCK;
export GPG_TTY=$(tty)
EOF
```

### Configure Yubikey

If you didn't touch your Yubikey yet, then PIN and admin PIN are following:

* PIN: 123456
* Admin PIN: 12345678

You are advised to change them later.

First setup your Yubikey as smart card using *gpg*.

```
gpg --card-edit
# enable admin access
gpg/card> admin
# setup card holder info
gpg/card> name
gpg/card> lang
gpg/card> login
gpg/card> sex
# hit enter and save changes
gpg/card> quit
```

### Generate GPG key

*TODO* - how to generate GPG key

### Move key to Yubikey

```
# pick key to edit
gpg --edit-key $KEY_ID

# pick AUTHZ sub key to be moved to the card
gpg> key $INDEX_AUTHZ_KEY
gpg> keytocard

# pick where should be key stored on card (Yubikey)

#Please select where to store the key:
#   (1) Signature key
#   (3) Authentication key
Your selection? 3
```

After restart it should all work. You can list available ssh keys (once Yubikey is inserted) using ``ssh-add -L``. If you have any key on the filesystem, you can add it to the GPG agent (since you no longer use standard SSH agent). Just you ``ssh-add ~/.ssh/id_rsa``. You will be asked for key password and then you twice enter new password (for GPG keyring). Then when you SSH to the machine, you are asked for GPG keyring password for you local SSH key or Yubikey/SmartCard PIN for SSH key on the card.
