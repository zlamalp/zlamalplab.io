---
layout: post
comments: false
categories: 
- personal
- xubuntu
---

Following instructions are highly personalized in order to install my preferred apps, themes, icon etc.

* Download ISO: [https://unit193.net/xubuntu/core/](https://unit193.net/xubuntu/core/)
* Burn and install as usual.

<br />

#### Install base applications
---

* Install daily productivity apps

<pre class="codebox nohighlight">
sudo apt-get install atril cherrytree engrampa firefox fish font-manager gdebi geoclue gigolo gimp gnome-calculator gparted gtkhash gufw gvfs-backends language-selector-gnome libreoffice-calc libreoffice-gtk3 libreoffice-style-elementary libreoffice-style-sifr libreoffice-writer menulibre mousepad mugshot pidgin pidgin-plugin-pack poedit redshift redshift-gtk ristretto simple-scan system-config-printer-gnome thunar thunar-archive-plugin thunar-gtkhash thunar-media-tags-plugin thunderbird ufw update-manager vlc xfburn xubuntu-restricted-extras
</pre>

* Install Czech locale

<pre class="codebox nohighlight">
sudo apt-get install firefox-locale-cs libreoffice-help-cs libreoffice-l10n-cs thunderbird-locale-cs
</pre>

* Install Sayonara music player

<pre class="codebox nohighlight">
sudo apt-add-repository ppa:lucioc/sayonara
sudo apt-get update
sudo apt-get install sayonara
</pre>

* Install AppGrid (since USC a GSC sucks)

<pre class="codebox nohighlight">
sudo apt-add-repository ppa:appgrid/stable
sudo apt-get update
sudo apt-get install appgrid
</pre>

* Fix update-manager window size (outside Unity)

<pre class="codebox nohighlight">
sudo apt-get install wmctrl
update-manager
wmctrl -F -r "Správce aktualizací" -e 0,0,10,600,700
</pre>

<br />

#### Install Themes
---

* Install my version of arc-theme (orange)

<pre class="codebox nohighlight">
sudo apt-get install autoconf automake libgtk-3-dev gtk2-engines-murrine gtk2-engines-pixbuf
git clone https://github.com/zlamalp/arc-theme
cd arc-theme
git checkout orange
./autogen.sh --prefix=/usr --disable-transparency --disable-cinnamon --disable-gnome-shell --disable-unity
sudo make install
</pre>

* Install my version of arc-firefox-theme (orange)

<pre class="codebox nohighlight">
sudo apt-get install automake autoconf
git clone https://github.com/zlamalp/arc-firefox-theme && cd arc-firefox-theme
git checkout orange
./autogen.sh --prefix=/usr
sudo make install
</pre>

<br />

#### Install Icons
---

* Install Ultra Flat icons

<pre class="codebox nohighlight">
sudo add-apt-repository ppa:noobslab/icons
sudo apt-get update
sudo apt-get install ultra-flat-icons-orange
</pre>

* Install Oranchelo icons

<pre class="codebox nohighlight">
sudo add-apt-repository ppa:oranchelo/oranchelo-icon-theme
sudo apt-get update
sudo apt-get install oranchelo-icon-theme
</pre>

* Inherit icons
    * Edit /usr/share/icons/Ultra-Flat-Orange/index.theme
    * Add inheritance to icons

<pre class="codebox nohighlight">
Inherits=Oranchelo,Numix,elementary-xfce-darker
</pre>

<br />

#### Install fonts
---

* Install *Source Code Pro* + *FontAwesome* and *Icons* fonts

<pre class="codebox nohighlight">
mkdir fonts-to-install && cd fonts-to-install
wget https://zlamalp.gitlab.io/downloads/FavoriteFonts.tar.gz
tar -x -f ./FavoriteFonts.tar.gz
rm ./FavoriteFonts.tar.gz
sudo cp ./* /usr/share/fonts/truetype/
cd ../
rm -r ./fonts-to-install
</pre>

<br />

#### Install xfce-windowck-plugin
---

<pre class="codebox nohighlight">
wget https://zlamalp.gitlab.io/downloads/xfce4-windowck-plugin_0.4.0_amd64.deb
sudo dpkg -i xfce4-windowck-plugin_0.4.0_amd64.deb
rm xfce4-windowck-plugin_0.4.0_amd64.deb
</pre>

<br />

#### Install fixed packages for Microsoft fonts and xfce-weather-plugin
---

<pre class="codebox nohighlight">
wget https://zlamalp.gitlab.io/downloads/ttf-mscorefonts-installer_3.6_all.deb
wget https://zlamalp.gitlab.io/downloads/xfce4-weather-plugin_0.8.9-1_amd64.deb
sudo dpkg -i ttf-mscorefonts-installer_3.6_all.deb
sudo dpkg -i xfce4-weather-plugin_0.8.9-1_amd64.deb
rm ttf-mscorefonts-installer_3.6_all.deb
rm xfce4-weather-plugin_0.8.9-1_amd64.deb
</pre>

<br />

#### Allow printer/scanner in UFW
---

<pre class="codebox nohighlight">
sudo ufw allow from 192.168.1.7
</pre>

<br />

#### Install Perun project development
---

<pre class="codebox nohighlight">
sudo apt-get install git maven apache2 openjdk-8-jdk php-cli postgresql postgresql-contrib visualvm
sudo apt-get install libswitch-perl liblwp-authen-negotiate-perl libjson-any-perl libtext-asciitable-perl libterm-readkey-perl libwww-perl libcrypt-ssleay-perl libtext-unidecode-perl libdate-calc-perl libnet-ldap-perl
sudo apt-get install dh-make equivs rpm
</pre>
