---
layout: post
comments: false
categories: 
- ubuntu-touch
- ubports
---

<p><img src="/downloads/ubports-droid-logo.png" width="200" align="left" style="padding: 0 10px 10px 0; box-shadow: none !important;" /> I got an old LG Nexus 5 phone so I could finally try ubuntu touch on a phone. It's so called <a href="https://ubports.com/page/ubuntu-nexus-5">core device</a>, which is fully supported by the ubports developers and all hardware should work. It has terrible battery life, but hey, its 3 years old with not replacable battery. I found pretty good <a href="https://forums.ubports.com/topic/420/successful-ubports-installation-and-contacts-import-on-nexus-5">install guide</a> at ubports forum. It worked like a charm and all was done in 15 minutes.</p>

### Installation

My phone was in a factory default state with *Android 6.0.1* (last update from Google for this device), so I switched it to developer mode. Located *Build number* item in *Settings -> About device/phone* and taped it 7 times.

I downloaded **Magic-Device-Tool** which is able to install ubuntu-touch on a phone. You can use: ``git clone https://github.com/MariusQuabeck/magic-device-tool.git`` to get it.

Then I booted my phone to the bootloader (fastboot mode) by pressing Volume up and down together with Power button. It stated, that device is locked.

So I used **Magic-Device-Tool** to remove a lock on my phone by starting ``launcher.sh`` (you select device, operation unlock and connect the device by USB, read carefully what is displayed in a console).
Then phone asked, if I really wish to do this and once confirmed, it wiped out all personal data and set itself to factory default again. On boot it shows Google logo with unlocked lock icon.

After the reboot, I rebooted again to booloader (fastboot mode) and run ``launcher.sh`` again to install ubuntu-touch OS to the phone (legacy chanel). It downloaded all necessary files and installed it to the phone in 5 minutes.
After a few more restarts it booted to ubuntu touch OS !!