---
layout: post
comments: false
categories: GWT
---

GWT framework is really powerfull tool, to create functional enterprise web applications. But what was always painfull was doing a good design. Clean GWT provides only base widgets and where it helps developers to do the functional code, there it also prevent designers to create simple DOM with efective CSS.

There is a possibility to use UiBinder, which is meant to split design and functional part. DOM/Widgets structure is defined in XML file, running code in .JAVA file matches xml using anotations.

But this won't get you nice looking style and many common widgets. GWT is powerful in what it does. It wraps existing javascript to java. So you can actually wrap stuff like Bootstrap.

You can get pretty much of it here: http://gwtbootstrap.github.io/

Simply add following to your projects pom.xml:

```xml
        <dependencies>
            <dependency>
                <groupId>com.github.gwtbootstrap</groupId>
                <artifactId>gwt-bootstrap</artifactId>
                <version>2.3.2.1-SNAPSHOT</version>
            </dependency>
        </dependencies>

        <repositories>
            <repository>
                <id>sonatype</id>
                <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
            </repository>
        </repositories>
```

Then add following to your gwt.xml:

```xml
        <inherits name="com.github.gwtbootstrap.Bootstrap"/>

        <!-- if you wish responsive design -->
        <set-property name="bootstrap.responsiveDesign" value="true"/>
       <!-- If you wish to use GWT's UiBinder -->
       <inherits name="com.google.gwt.uibinder.UiBinder"/>
```

Then add following to your .html file before loading GWT module

```xhtml
        <!-- before your module(*.nocache.js) loading  -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--[if IE 7]>
        <link rel="stylesheet" href="[module_name]/css/font-awesome-ie7.css">
        <![endif]-->
```

GWT Bootstrap wraps version 2, it's meant to use with UiBinder, but most widgets have solid API and can be used without it. New widgets wraps original (Button, CellTable,...) and provide styling with respect to Bootstrap.

There are also other project, like this one: http://gwtbootstrap3.github.io/gwtbootstrap3-demo/  wich uses Bootstrap 3, but it's even more tightly binded to UiBinder usage. Widget names were also not so intuitive for me.

Anyway, by using Bootstrap libraries in your project, you can get good looking webapp.